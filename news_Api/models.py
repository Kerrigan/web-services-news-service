from django.db import models
from django.contrib.auth.models import User


# Author model, holds author name aswell as a foreign key to the users
class Author(models.Model):
    name = models.CharField(max_length=30)
    username = models.ForeignKey(User, on_delete=models.CASCADE)
    # I did have the password but i removed it as you mentioned we shouldn't store the password this way
    # password = models.CharField(max_length=50)

# Displays author name in place of AuthorObject in server admin view
    def __str__(self):
        return self.name


# NewsStory model holds story data
class NewsStory(models.Model):
    headline = models.CharField(max_length=64)
    story_categories = [('pol', 'Politics'), ('art', 'Art'), ('tech', 'Technology'), ('trivia', 'Trivia')]
    category = models.CharField(choices=story_categories, max_length=10)
    news_regions = [('uk', 'UK News'), ('eu', 'European News'), ('w', 'World News')]
    region = models.CharField(choices=news_regions, max_length=13)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    date = models.DateField()
    details = models.CharField(max_length=512)

# Displays headline in place of NewsStoryObject in server admin view
    def __str__(self):
        return self.headline
