from django.contrib.auth import authenticate, login, logout
from django.contrib import auth
import json
from news_Api.models import NewsStory
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime


@csrf_exempt
def handle_login_request(request):
    if request.method == 'POST':
        # Get user details from POST request
        user_name = request.POST['username']
        user_pass = request.POST['password']

        # Authenticate the user
        user = authenticate(request, username=user_name, password=user_pass)
    else:
        return HttpResponse('Invalid request')

    # Login user and checks they were successfully authenticated
    if user is not None:
        if user.is_active:
            # Log user in and check user is authenticated
            login(request, user)
            request.session['username'] = user_name

            if user.is_authenticated:
                # Returns status 200 when user successfully logs in
                return HttpResponse(user.username + ' welcome back!', status=200)

            else:
                # Returns status 401 unauthorised if user is not authorised
                return HttpResponse('User not authenticated', status=401)

        else:
            # Returns status 400 account disabled if user account is not active
            return HttpResponse('Disabled account', status=400)

    else:
        # Returns status 400 bad request if request is not of type POST
        return HttpResponse('Login Failed ', status=400)


@csrf_exempt
def handle_logout_request(request):
    if request.method == 'POST':
        logout(request)
        # Returns status 200 when user successfully logs out
        return HttpResponse('logged out, Goodbye', status=200)
    else:
        # Returns status 400 bad request if request is not of type POST
        return HttpResponse('Bad Request!', status=400)


@csrf_exempt
def post_story(request):
    if request.method == 'POST':

        # Retrieve the active user from the request
        user = auth.get_user(request)

        if user.is_authenticated:
            content = json.loads(request.body)

            # Get authorised users author name to set author of story
            username = user.author_set.all()[0]
            curr_date = datetime.now()

            # Creates the news story
            story = NewsStory(headline=content['headline'], category=content['category'], region=content['region'], details=content['details'], author=username, date=curr_date)
            story.save()

            # Returns 201 when story is posted successfully
            return HttpResponse('Story posted successfully', status=201)

        else:
            # Returns status 503 service unavailable if user is not autorised
            return HttpResponse('Author not authorised!', status=503)

    else:
        # Returns status 503 invalid request if request is not of type POST
        return HttpResponse('Invalid request', status=503)


def get_story(request):
    content = json.loads(request.body)

    # If tag is not specified by user it defaults to .* to not filter stories
    if content['story_cat'] == '*':
        category = '.*'
    else:
        category = content['story_cat']

    if content['story_region'] == '*':
        region = '.*'
    else:
        region = content['story_region']

    # Filters news stories with user specified filters
    if content['story_date'] == '*':
        results = NewsStory.objects.filter(category__iregex=category, region__iregex=region)

    else:
        # Filters news stories with correct date format
        story_date = datetime.strptime(content['story_date'], '%d/%m/%Y')
        results = NewsStory.objects.filter(category__iregex=category, region__iregex=region, date__gt=story_date.strftime('%Y-%m-%d'))

    if results is not None:

        stories = []

        for story in results:
            stories.append(
                {"key": story.id, "headline": story.headline, "story_cat": story.category, "story_region": story.region,
                 "author": story.author.name, "story_date": story.date.isoformat(), "story_details": story.details})

        payload = json.dumps({"stories": stories})

        # Returns status 200 when story is successfully created
        return HttpResponse(payload, status=200)

    else:
        # Returns status 404 when no stories are found from the news directory
        return HttpResponse("No news stories found!", status=404)


@csrf_exempt
def delete_story(request):
    # Retrieve user details from the request and check they are authenticated
    user = auth.get_user(request)
    if user.is_authenticated:
        content = json.loads(request.body)

        story_key = content['story_key']

        # If story does not exist an exception is thrown, an error message is sent to the client
        try:
            story = NewsStory.objects.get(id=story_key)

            story.delete()

            # Returns 201 when story is deleted successfully
            return HttpResponse(status=201)

        except Exception as e:
            print(e)
            story = "Story specified does not exist"

            # Returns 503 status service unavailable when story not found
            return HttpResponse(story, status=503)

    else:
        # Returns 503 status service unavailable when user not authorised
        return HttpResponse('User not authorised!', status=503)
